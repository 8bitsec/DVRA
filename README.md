# DVRA - Damn Vulnerable Ruby App

A Ruby (on Rails) application vulnerable to OWASP Top 10.

### Installation

The easiest way to run DVRA is cloning the repository and use Docker.

```sh
$ cd DVRA/
$ docker image build -t dvra:1.0 . 
$ docker container run --publish 3000:3000 --detach --name dvra-container dvra:1.0
```

Then just browse to http://localhost:3000/

![DVRA](DVRA.png "DVRA")
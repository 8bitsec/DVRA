class InjectionController < ApplicationController
  def injection
    if cookies[:header].nil?
      cookies[:header] = "BAh7BkkiCGNtZAY6BkVUSSIJZGF0ZQY7AFQ="
    end
    c = Marshal.load(Base64.decode64(cookies[:header]))['cmd']
    @current_date = `#{c}`
    if request.post?
      User.joins('WHERE username IS "' + params[:username] + '"')[0].nil? ? @user = "" : @user = User.joins('WHERE username IS "' + params[:username] + '"')[0]["username"]
    end 
  end
end

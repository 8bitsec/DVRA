class XmlExternalEntityController < ApplicationController
  def xml_external_entity
    @content = ""
    if request.post?
      options = Nokogiri::XML::ParseOptions::NOENT   # Needed for the external DTD to be loaded
      xml = Nokogiri::XML(params[:file].read, nil, nil, options)
      @content = xml.to_xml(indent: 4)
    end
  end
end

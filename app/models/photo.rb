class Photo < ApplicationRecord
  #Mounts paperclip image
  has_attached_file :image
end

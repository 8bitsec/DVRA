Rails.application.routes.draw do
  root 'index#index'
  get 'index/index'
  get 'injection' => 'injection#injection'
  post 'injection' => 'injection#injection'
  get 'broken_authentication' => 'broken_authentication#broken_authentication'
  post 'broken_authentication' => 'broken_authentication#broken_authentication'
  get 'sensitive_data_exposure' => 'injection#injection'
  get 'xml_external_entity' => 'xml_external_entity#xml_external_entity'
  post 'xml_external_entity' => 'xml_external_entity#xml_external_entity'
  get 'broken_access_control' => 'broken_access_control#broken_access_control'
  post 'broken_access_control' => 'broken_access_control#broken_access_control'
  get 'security_misconfiguration' => 'photos#new'
  get 'cross_site_scripting' => 'cross_site_scripting#cross_site_scripting'
  get 'insecure_deserialization' => 'injection#injection'
  get 'known_vulnerabilities' => 'photos#new'
  resources :photos
  get 'insufficient_logging_monitoring' => 'insufficient_logging_monitoring#insufficient_logging_monitoring'
end
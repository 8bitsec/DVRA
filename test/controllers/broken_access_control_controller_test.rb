require 'test_helper'

class BrokenAccessControlControllerTest < ActionDispatch::IntegrationTest
  test "should get broken_access_control" do
    get broken_access_control_broken_access_control_url
    assert_response :success
  end

end

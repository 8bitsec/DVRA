require 'test_helper'

class InsecureDeserializationControllerTest < ActionDispatch::IntegrationTest
  test "should get insecure_deserialization" do
    get insecure_deserialization_insecure_deserialization_url
    assert_response :success
  end

end

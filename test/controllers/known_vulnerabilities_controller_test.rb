require 'test_helper'

class KnownVulnerabilitiesControllerTest < ActionDispatch::IntegrationTest
  test "should get known_vulnerabilities" do
    get known_vulnerabilities_known_vulnerabilities_url
    assert_response :success
  end

end

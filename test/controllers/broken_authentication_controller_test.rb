require 'test_helper'

class BrokenAuthenticationControllerTest < ActionDispatch::IntegrationTest
  test "should get broken_authentication" do
    get broken_authentication_broken_authentication_url
    assert_response :success
  end

end

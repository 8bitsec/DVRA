FROM ruby:2.7.0-alpine3.11
RUN apk add --update --no-cache bash build-base nodejs sqlite-dev yarn tzdata nodejs-npm python2
RUN npm install --save-dev webpack@4.0.0
RUN npm install --save-dev webpack-cli
RUN gem install bundler:1.17.2
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install --check-files
COPY Gemfile* ./
RUN bundle install
COPY . .
ENV PATH=./bin:$PATH
EXPOSE 3000
# Start the main process.
CMD ["rails", "server"]